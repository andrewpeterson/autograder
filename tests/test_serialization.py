"""
Tests to make sure that data can be converted to ascii and
back to pass over html.
"""

import numpy as np
from submission.utilities import serialize_data, deserialize_data


def test_single_np_array():
    """Case I: A single numpy array."""
    A = np.random.random((5, 3))
    encoded = serialize_data(A)
    decoded = deserialize_data(encoded)
    assert (decoded == A).all()


def test_float():
    """Case II: A float"""
    a = float(np.random.random())
    encoded = serialize_data(a)
    decoded = deserialize_data(encoded)
    assert a == decoded


def test_generic():
    """Case III: A string as generic python object."""
    s = 'encyclopedia'
    encoded = serialize_data(s)
    decoded = deserialize_data(encoded)
    assert s == decoded


def test_embedded_dict():
    """Case IV: A dictionary with many sub-types."""
    d = {'A': np.random.random((4, 2)),
         'a': float(np.random.random()),
         's': 'two words'}
    encoded = serialize_data(d)
    decoded = deserialize_data(encoded)
    assert (d['A'] == decoded['A']).all()
    assert d['a'] == decoded['a']
    assert d['s'] == decoded['s']


if __name__ == '__main__':
    test_single_np_array()
    test_float()
    test_generic()
    test_embedded_dict()
