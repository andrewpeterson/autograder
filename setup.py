from setuptools import setup, find_packages


setup(
    name='submission',
    version='0.8.0',
    description='Tools to submit and grade problem sets.',
    author='Andrew Peterson',
    packages=find_packages(exclude=['examples']),
    install_requires=['flask', 'pytz', 'numpy!=1.16.0', 'simplejson', 'six',
                      'requests'],
)

# Be sure the version number is updated both here and in
# submission/__init__.py

# Note that numpy 1.16.0 has a bug related to pickling which means older
# versions can't unpickle the data. This makes data transmission impossible
# between versions. May one day switch to msgpack-numpy as alternative
# encoder.
