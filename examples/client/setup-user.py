from submission.client import setup


setup()

"""
Note:
You should be provided the server (http address), user, and password from
the person who runs the server.

If running a test copy locally, you most likely want to use the following
test parameters:
    server = http://127.0.0.1:5000
    user = user1
    password = hello
"""
