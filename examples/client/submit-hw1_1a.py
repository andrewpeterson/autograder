from submission.client import submit


def hello(word):
    return 'Hello ' + word + '!'


submit(function=hello, assignment='hw1_1a')
