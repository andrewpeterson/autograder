from submission.client import submit


def get_sign(x):
    if x >= 0.:
        return +1
    else:
        return -1


submit(function=get_sign,
       assignment='hw1_1b')
