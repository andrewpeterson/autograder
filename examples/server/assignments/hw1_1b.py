import numpy as np


points = 5.


def true_function(x):
    if x >= 0.:
        return +1
    else:
        return -1


def random_input():
    """Creates random input for this assignment."""
    x = np.random.rand() - 0.5
    return {'x': x}


def compare(answer, correct_answer):
    result = answer == correct_answer
    message = ''
    if result is True:
        score = points
    else:
        score = 0.
        message = ('Your answer was "{}". The correct answer was "{}".'
                   .format(answer, correct_answer))
    return result, message, score
