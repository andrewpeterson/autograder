import numpy as np
from submission.utilities import (compare_arrays, randomize_number,
                                  handle_class, compare_floats)


points = 1.


class TrueClass:

    def __init__(self, coefficients):
        self.coefficients = coefficients

    def get_value(self, x):
        value = 0.
        for power, c in enumerate(reversed(self.coefficients)):
            value += c * x**power
        return value


def true_function(**inputdata):
    result = handle_class(TrueClass, inputdata)
    return result


def random_input():
    """Creates random input for this assignment."""
    n = np.random.randint(3, 8)
    coefficients = []
    for _ in range(n):
        coefficients.append(np.random.rand() * 20. - 10.)
    x = np.random.rand() * 20. - 10.
    d = {':class-methods:': ['get_value'],
         '__init__': {'coefficients': coefficients},
         'get_value': {'x': x}}
    return d


def compare(answer, correct_answer):
    result, message = compare_floats(answer, correct_answer)
    score = {True: points, False: 0.}[result]
    return result, message, score
