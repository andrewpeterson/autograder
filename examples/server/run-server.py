from submission.server import WebGrader


webgrader = WebGrader()

webgrader.run(start_app=True)
# Typically set start_app to False on a service such as pythonanywhere.com
# Leave as True to run a local test server.
