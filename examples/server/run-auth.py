from submission.server import Authenticator

auth = Authenticator()

# Add a user. Password will be requested at prompt, but could
# be fed as a second keyword to `add_user`.
auth.add_user('user1')

# Check that it worked. Use the same password as in the example
# script.
result = auth.check_user('user1')
print(result)
