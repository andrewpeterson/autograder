import os
import sys
import time
import re
import json
import pkgutil
import importlib
from flask import Flask, request
from importlib import import_module
from getpass import getpass
from werkzeug.security import generate_password_hash, check_password_hash

from . import __version__
from .utilities import Logger, serialize_data, deserialize_data


class WebGrader:
    """
    Web server to automatically accept and grade homework assignments.

    Parameters
    ==========
    datadir : str
        Directory that holds assignments in a subdirectory 'assignments'.
        If authenticator, scorekeeper, and logs are not specified they are
        put here too. If none is specified, uses the working directory.
        Note that this will put the datadir on your PYTHONPATH.
    timezone : str
        Timezone to be used as a default in reporting scores to students.
        Must be in a format expected by pytz, e.g., 'America/New_York'.
    authenticator : instance
        An Authenticator instance (optional).
    scorekeeper : instance
        A ScoreKeeper instance (optional).
    log : instance
        A .utilities.Logger instance (optional).
    trueanswers : instance
        A TrueAnswers instance (optional).
    """

    def __init__(self, datadir=None, timezone='America/New_York',
                 authenticator=None, scorekeeper=None, log=None,
                 trueanswers=None):
        if datadir is None:
            datadir = os.getcwd()
        else:
            sys.path.insert(0, datadir)
        if log is None:
            log = Logger(os.path.join(datadir, 'submission.log'))
        if authenticator is None:
            authenticator = Authenticator(userfile=os.path.join(datadir,
                                                                'users.txt'),
                                          log=log)
        if scorekeeper is None:
            scorekeeper = ScoreKeeper(path=os.path.join(datadir, 'scores'))
        if trueanswers is None:
            trueanswers = TrueAnswers(path=os.path.join(datadir,
                                                        'true-answers'))
        self._trueanswers = trueanswers
        self._timezone = timezone
        self._datadir = datadir
        self._auth = authenticator
        self._score = scorekeeper
        self.log = log
        print('Logging to {}'.format(self.log.file.name))
        self.log('Initiated. {}'.format(time.ctime()))
        self.log(' Version: {}'.format(__version__))
        self.log(' Timezone set to: {}'.format(timezone))

    def _get_submission(self):
        """Function to initiate turning in a homework problem
        in conjunction with the student's submit function.
        There must be an assignment named assignment in the assignments
        folder.
        """
        user = request.authorization['username']
        self.log('-'*10)
        self.log('In _get_submission.')
        allowed = self._auth.check_user(request.authorization['username'],
                                        request.authorization['password'])
        self.log('User {} login successful: {}'.format(user, str(allowed)))
        if allowed is False:
            return 'Error: Invalid username and/or password.'
        content = request.json
        self.log(content)
        if content['__version__'] != __version__:
            return ('Error: Wrong software version. You are running version'
                    ' {} of the file submission software while the server is'
                    ' using version {}. Please update your local version '
                    'before submitting.'
                    .format(content['__version__'], __version__))
        assignment = content['assignment']
        self.log('assignment: {}'.format(assignment))
        # Import the local version of the problem.
        module_location = 'assignments.{}'.format(assignment)
        try:
            problem = import_module(module_location)
        except:
            return 'Error: {} not found.'.format(module_location)
        self.log('done with import of {}'.format(assignment))
        random_input = problem.random_input()
        self.log('got random input')
        self.log(random_input)
        correct_answer = problem.true_function(**random_input)
        self.log('got true answer')
        self._trueanswers.set(assignment, user, correct_answer)

        self.log('true answer: {}'.format(str(correct_answer)))
        return serialize_data(random_input)

    def _get_answer(self):
        """Function to take the answer from the user (via POST)
        and check its solution."""
        user = request.authorization['username']
        self.log('-'*10)
        self.log('In _get_answer.')
        self.log(user)
        allowed = self._auth.check_user(request.authorization['username'],
                                        request.authorization['password'])
        self.log('User {} login successful: {}'.format(user, str(allowed)))
        if allowed is False:
            return 'Invalid username/password.'
        content = deserialize_data(request.form['serialized'])
        self.log(content)
        assignment = content['assignment']
        self.log(assignment)
        module_location = 'assignments.{}'.format(assignment)
        try:
            problem = import_module(module_location)
        except:
            return '{} not found.'.format(module_location)
        answer = content['answer']
        self.log('content received: {}'.format(str(answer)))
        correct_answer = self._trueanswers.get(assignment, user)
        result, message, score = problem.compare(answer, correct_answer)
        self.log('result: {}'.format(str(result)))
        self.log('source:')
        self.log(str(content['source']))
        self._score.mark_score(user, assignment, score)
        return json.dumps([result, message])

    def _check_login(self):
        """Just makes sure that a user can succesfully be authenticated."""
        allowed = self._auth.check_user(request.authorization['username'],
                                        request.authorization['password'])
        if allowed:
            return 'Authenticated.'
        else:
            return 'Invalid username/password.'

    def _check_scores(self):
        user = request.authorization['username']
        allowed = self._auth.check_user(request.authorization['username'],
                                        request.authorization['password'])
        if allowed is False:
            return 'Invalid username/password.'
        content = request.json
        assignment = content['assignment']
        score = self._score.get_score(user, assignment,
                                      items={'timezone': self._timezone})
        return json.dumps(score)

    def _check_status(self):
        """Just returns a generic message to show the server is running."""
        return ('Autograder version {}: All systems normal...'
                .format(__version__))

    def run(self, start_app=True):
        """Sets up the flask app and starts it.
        If start_app is False, returns an instance of the app
        rather than starting it.
        """
        self.webapp = Flask(__name__)
        self.webapp.add_url_rule(rule='/submit/',
                                 endpoint='get_submission',
                                 view_func=self._get_submission,
                                 methods=['GET', 'POST'])
        self.webapp.add_url_rule(rule='/answer/',
                                 endpoint='get_answer',
                                 view_func=self._get_answer,
                                 methods=['GET', 'POST'])
        self.webapp.add_url_rule(rule='/checklogin/',
                                 endpoint='checklogin',
                                 view_func=self._check_login)
        self.webapp.add_url_rule(rule='/check_scores/',
                                 endpoint='check_scores',
                                 view_func=self._check_scores,
                                 methods=['GET', 'POST'])
        self.webapp.add_url_rule(rule='/',
                                 endpoint='/',
                                 view_func=self._check_status)

        if start_app:
            self.webapp.run(debug=True)
        else:
            return self.webapp


class ScoreKeeper:
    """For keeping track of students' points on various problems."""

    def __init__(self, path='scores'):
        if not os.path.exists(path):
            os.mkdir(path)
        self.path = path
        self.find_possible_points()

    def mark_score(self, user, assignment, score):
        """Marks the socre for the user with name <user> for
        assignment named <assignment>."""
        userpath = os.path.join(self.path, make_valid_filename(user))
        line = '{}\t{}\t{}'.format(assignment, score, str(time.time()))
        with open(userpath, 'a') as f:
            f.write(line + '\n')

    def get_score(self, user, assignment, items=None):
        """Reads the log file to find the (highest) score for the named
        assignment and specified user. To get all assignments use
        assignment=''. Also returns the possible points on this assignment.
        If 'items' (a dictionary) is supplied, it also includes these
        entries in the returned dictionary. (This is meant for the
        timezone.)
        """
        userpath = os.path.join(self.path, make_valid_filename(user))
        if assignment == '':
            keys = sorted(list(self.possible_points.keys()))
            scores = []
            for key in keys:
                scores.append(self.get_score(user, key, items))
            return scores

        try:
            with open(userpath, 'r') as f:
                lines = f.read().splitlines()
        except FileNotFoundError:
            lines = []
        maxscore = None
        time_complete = None
        for line in lines:
            line_assignment, line_score, line_time = (line.split('\t'))
            if line_assignment == assignment:
                if maxscore is None:
                    maxscore = 0.
                if float(line_score) > maxscore:
                    maxscore = float(line_score)
                    time_complete = float(line_time)
        result = {'points': maxscore,
                  'possible': self.possible_points[assignment],
                  'time complete': time_complete,
                  'assignment': assignment}
        if items is not None:
            result.update(items)
        return result

    def find_possible_points(self):
        """Looks in the assignments module folder for all assignments
        that have possible points and returns a dictionary of these.
        """
        possible_points = {}
        import assignments
        for importer, name, _ in pkgutil.iter_modules(assignments.__path__):
            # New convoluted way of doing this - would import_module work?
            module_spec = importer.find_spec(name)
            this_problem = importlib.util.module_from_spec(module_spec)
            module_spec.loader.exec_module(this_problem)
            possible_points[name] = this_problem.points
        self.possible_points = possible_points

    def find_active_users(self):
        """Returns a list of users with scores in the current directory."""
        return os.listdir(self.path)


class Authenticator:
    """Stores and compares user names and passwords with those in a database.
    """

    def __init__(self, userfile='users.txt', log=None):
        if log is None:
            log = Logger()
        self.log = log
        self.users = {}
        if os.path.exists(userfile):
            with open(userfile, 'r') as f:
                lines = f.readlines()
            for line in lines:
                username, hashed, time = line.split('\t')
                self.users[username] = hashed
                self.log(username)
        self._userfile = userfile

    def add_user(self, username, password=None):
        """Adds or updates the user and password to the database."""
        self.log('Adding user {}.'.format(username))
        if password is None:
            password = getpass()
        hashed = generate_password_hash(password)
        line = '{}\t{}\t{}'.format(username, hashed, str(time.time()))
        with open(self._userfile, 'a') as f:
            f.write(line + '\n')
        self.users[username] = hashed
        self.log('Added user {}.'.format(username))

    def check_user(self, username, password=None):
        """Checks if the username is that in the database."""
        self.log('Authenticator.check_user')
        if password is None:
            password = getpass()
        self.log('Checking username: {}'.format(username))
        if username not in self.users:
            self.log('Bad username.')
            return False
        self.log('Good username.')
        return check_password_hash(self.users[username], password)

    def get_users(self):
        """Returns all authorized users."""
        return list(self.users.keys())


def make_valid_filename(text):
    """Converts string at text into a valid filename.
    This is primarily to prevent malicious code from modifying files.
    These lines are modified from the django project."""
    text = str(text).strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', text)


class TrueAnswers:
    """On-disk file database to store true answers.
    This is necessary for servers where multiple threads are
    handling requests, so memory is not shared."""

    def __init__(self, path='true-answers'):
        if not os.path.exists(path):
            os.mkdir(path)
        self.path = path

    def _makepath(self, assignment, user):
        filename = '{}.{}'.format(make_valid_filename(assignment),
                                  make_valid_filename(user))
        path = os.path.join(self.path, filename)
        return path

    def set(self, assignment, user, correct_answer):
        """Stores the true answer to a file within the data storage."""
        data = serialize_data(correct_answer)
        path = self._makepath(assignment, user)
        with open(path, 'w') as f:
            f.write(data)

    def get(self, assignment, user):
        """Retrieves the true answer from the named file."""
        path = self._makepath(assignment, user)
        with open(path, 'r') as f:
            data = f.read()
        return deserialize_data(data)
