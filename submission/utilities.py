import numpy as np
import json
import base64
import datetime
import pytz
from . import __version__


class Logger:
    """Simple logging utility to write to a text file."""

    def __init__(self, filename='submission.log'):
        self.file = open(filename, 'a')

    def __call__(self, message):
        self.file.write(str(message) + '\n')
        self.file.flush()

    def __del__(self):
        self.file.close()


def serialize_data(data):
    """Creates a serialized version of data that can be passed via html
    POST; that is, as ASCII text. This is necessary as numpy.ndarray's
    cannot be turned directly into json. If you want to embed multiple
    numpy arrays, it must be in a dictionary, not a list. (The multiple
    dictionary items may be of mixed type, e.g., numpy arrays, strings,
    floats, ...).

    Parameters
    ==========
    data : many types
        Either (1) a native python object (like float, int, string, bool,
        or a dict, tuple, or list of these), or (2) a numpy array (type
        numpy.ndarray), or (3) a dictionary containing any combination of
        (1) and (2).
    """
    if isinstance(data, dict):
        newdata = {}
        for key, value in data.items():
            newdata[key] = serialize_data(value)
        entry = {'type': 'dict',
                 'value': newdata}
    elif isinstance(data, np.ndarray):
        encoded = data.tobytes()  # bytes, but loses shape
        encoded = base64.b64encode(encoded)  # ascii-like bytes
        encoded = encoded.decode('ascii')  # actual ascii
        value = {'data': encoded,
                 'dtype': str(data.dtype),
                 'shape': data.shape}
        entry = {'type': 'numpy.ndarray',
                 'value': value}
    elif isinstance(data, float):
        # Hexadecimal avoids tiny rounding errors.
        entry = {'type': 'float',
                 'value': data.hex()}
    else:  # Hope it's a generic python object!
        entry = {'type': 'generic',
                 'value': data}
    return json.dumps(entry)


def deserialize_data(serialized_data):
    """Reverses the output of serialize_data to re-create the input
    data."""
    entry = json.loads(serialized_data)
    if entry['type'] == 'dict':
        data = {}
        encodeddata = entry['value']
        for key, value in encodeddata.items():
            data[key] = deserialize_data(value)
    elif entry['type'] == 'numpy.ndarray':
        value = entry['value']
        encoded = value['data']  # ascii
        encoded = base64.b64decode(encoded)  # bytes
        data = np.frombuffer(encoded, dtype=value['dtype'])  # numpy.ndarray
        data = data.reshape(value['shape'])
    elif entry['type'] == 'float':
        data = float.fromhex(entry['value'])
    elif entry['type'] == 'generic':
        data = entry['value']
    else:
        raise NotImplementedError('Unknown type to deserialize: {}'
                                  .format(entry['type']))
    return data


def get_version():
    """Returns the version number."""
    return __version__


def compare_floats(answer, correct_answer, tol=1e-10):
    """Tool to compare two floating-point numbers to see if they match within
    a tolerance. Returns True/False depending on if they match, and a
    useful message if they do not."""
    residual = abs(float(answer) - float(correct_answer))
    if residual < tol:
        return True, ''
    message = ('Your answer was {}; the correct answer was {}. These '
               'values differ by {}, while the maximum tolerated '
               'difference is {}.')
    message = message.format(str(answer), str(correct_answer),
                             str(residual), str(tol))
    return False, message


def compare_arrays(answer, correct_answer, tol=1e-10):
    """Tool to compare two numpy arrays to see if they match within
    a per-element tolerance. Returns True/False depending on if they match,
    and a useful message if they do not."""
    if not hasattr(answer, 'shape'):
        message = ('I was expecting a numpy array. The object you submitted '
                   'does not appear to be a numpy array.')
        return False, message
    if answer.shape != correct_answer.shape:
        message = ('Your array is the wrong shape. I was expecting an array'
                   ' of shape {}; your array was shape {}.'
                   .format(str(correct_answer.shape), str(answer.shape)))
        return False, message
    max_residual = np.max(abs(answer - correct_answer))
    if max_residual < tol:
        return True, ''
    message = ('Your answer was:\n{}\nThe correct answer was\n{}\n'
               'The largest residual of any element was {}, which is '
               'larger than the tolerance of {}.'
               .format(str(answer), str(correct_answer),
                       str(max_residual), str(tol)))
    return False, message


def randomize_number(x, delta=0.1):
    """Adds random noise to the number x, not to exceed +/- delta*x.
    """
    noise = (np.random.random() - 0.5) * delta * x
    return x + noise


def get_formatted_time(timefloat, timezone):
    """Returns a nicely formatted string from a time stamp that was
    produced with time.time(), which is a float. Assumes that the
    timefloat is in the UTC timezone. The specified timezone should be in
    the format expected by pytz.timezone, e.g, 'America/New_York'.
    """
    utc = pytz.timezone('UTC')
    dt = datetime.datetime.utcfromtimestamp(timefloat)
    dt = utc.localize(dt)
    dt = dt.astimezone(pytz.timezone(timezone))
    return dt.strftime('%a %d-%b %H:%M:%S')


def make_score_line(score):
    """Takes in the dictionary produced by
    ScoreKeeper.get_score and converts it into a string formatted for display.
    """
    nodateformat = '{:12s}: {:5.1f} / {:5.1f}'
    dateformat = '{:12s}: {:5.1f} / {:5.1f}  ({:s})'
    missingformat = '{:12s}: {:5s} / {:5.1f}'
    if score['points'] is None:
        line = missingformat.format(score['assignment'], '', score['possible'])
    elif score['time complete'] is None:
        line = nodateformat.format(score['assignment'], score['points'],
                                   score['possible'])
    else:
        line = dateformat.format(score['assignment'], score['points'],
                                 score['possible'],
                                 get_formatted_time(score['time complete'],
                                                    score['timezone']))
    return line


def handle_class(StudentClass, inputdata):
    """Translates the input data into form expected by the class
    and runs the student's work with it."""
    instance = StudentClass(**inputdata['__init__'])
    result = {}
    for methodname in inputdata[':class-methods:']:
        method = getattr(instance, methodname)
        result[methodname] = method(**inputdata[methodname])
    return result
