import requests
import os
import inspect
from getpass import getpass
from configparser import ConfigParser
from six.moves import input
from simplejson.scanner import JSONDecodeError

from . import __version__
from .utilities import (serialize_data, deserialize_data, make_score_line,
                        handle_class)


def submit(function, assignment, server=None, user=None, password=None):
    """Function to submit a function as a homework assignment.

    Parameters
    ==========
    function : function object
        The student's callable function or other python object, as assigned.
    assignment : string
        The label of the assignment.
    server : string
        The web address of the server. This may optionally be contained
        in a configuration file created by the setup utility, or if not
        supplied the user will be prompted.
    user : string
        The username of the student. This may optionally be contained
        in a configuration file created by the setup utility, or if not
        supplied the user will be prompted.
    password : string
        The password of the student. This may optionally be contained
        in a configuration file created by the setup utility, or if not
        supplied the user will be prompted.
    """
    server, user, password = _get_login_info(server, user, password)
    # Query server for random input for this assignment.
    url = '/'.join([server, 'submit']) + '/'
    post = {'assignment': assignment,
            '__version__': __version__}
    response = requests.post(url=url, json=post, auth=(user, password))
    if response.text.startswith('Error:'):
        raise RuntimeError(response.text)
    inputdata = deserialize_data(response.text)
    print('Input data:')
    print(inputdata)

    # Take different action depending on if we have class or function.
    assignment_type = 'function'
    if type(inputdata) is dict:
        if ':class-methods:' in inputdata:
            print('Class expected.')
            assignment_type = 'class'
            functionresult = handle_class(function, inputdata)
    if assignment_type == 'function':
        # Run the student's function.
        functionresult = function(**inputdata)
    print('Your result:')
    print(functionresult)

    # Send result to server to check.
    url = '/'.join([server, 'answer']) + '/'
    post = {'answer': functionresult,
            'assignment': assignment,
            'user': user,
            'source': _get_source(function)}
    post = {'serialized': serialize_data(post)}
    response = requests.post(url=url, data=post, auth=(user, password))
    try:
        outcome, message = response.json()
    except JSONDecodeError:
        print("There was an error trying to decode the server's response, "
              "which was:")
        print(response.text)
        return
    print(["Incorrect answer.", "Correct!"][outcome])
    if len(message) > 0:
        print(message)


def setup():
    """This function can be run to validate that a login is working
    as well as to create a configuration file to save login information."""
    server = input('Enter server: ')
    server = server.rstrip('/')  # Any trailing /'s.
    user = input('Enter user: ')
    password = getpass()
    url = '/'.join([server, 'checklogin'])
    response = requests.get(url, auth=(user, password))
    print(response.text)
    if response.text != 'Authenticated.':
        print('Was not able to log in.')
        return
    here = 'submission.cfg'
    home = os.path.join(os.path.expanduser('~'), '.submission.cfg')
    print(_setuptext.format(homeconfig=str(home)))
    choice = input('Enter choice: ')
    if choice == '1':
        filename = here
    elif choice == '2':
        filename = home
    else:
        print('Doing nothing.')
        return
    print(_setuptext2)
    choice = input('Enter choice: ')
    if choice == '1':
        store_password = True
    else:
        store_password = False
    print('Creating file: {}'.format(filename))
    if os.path.exists(filename):
        raise RuntimeError('File exists!\n'
                           'If you really want to overwrite it, delete it '
                           'then re-run this script.')
    configdict = {'server': server,
                  'user': user}
    if store_password:
        configdict['password'] = password
    config = ConfigParser()
    config['DEFAULT'] = configdict
    with open(filename, 'w') as f:
        config.write(f)


def check_score(assignment='', server=None, user=None, password=None):
    """Checks the users scores. Provide the assignment name
    or leave blank to get all scores to date.
    """
    server, user, password = _get_login_info(server, user, password)
    url = '/'.join([server, 'check_scores']) + '/'
    post = {'assignment': assignment}
    response = requests.post(url=url, json=post, auth=(user, password))
    scores = response.json()
    if type(scores) is list:
        for score in scores:
            print(make_score_line(score))
    else:
        print(make_score_line(scores))


def _get_login_info(server, user, password):
    """Figures out the server, user, and password from combination
    of supplied keywords, settings files, and, if needed, direct user
    input."""
    if None in [server, user, password]:
        # Look for values in the config file if some are missing.
        cserver, cuser, cpassword = _read_config()
    server = cserver if server is None else server
    user = cuser if user is None else user
    password = cpassword if password is None else password
    if server is None:
        server = input('Enter server: ')
    if user is None:
        user = input('Enter user: ')
    if password is None:
        password = getpass()
    return server, user, password


def _read_config():
    """Attempts to load settings from configuration file.
    Looks first for submission.cfg in the current directory, then
    for $HOME/.submission.cfg.
    """
    here = 'submission.cfg'
    home = os.path.join(os.path.expanduser('~'), '.submission.cfg')
    if os.path.exists(here):
        filename = here
    elif os.path.exists(home):
        filename = home
    else:
        # No config file exists.
        return None, None, None
    config = ConfigParser()
    config.read(filename)
    section = config['DEFAULT']
    values = [section.get(key, None)
              for key in ('server', 'user', 'password')]
    return values


def _get_source(function):
    """Gets a copy of the source code of the calling function and script."""
    functionsource = inspect.getsource(function)
    caller = inspect.stack()[-1]
    filename = caller.filename
    try:
        with open(filename, 'r') as f:
            filesource = f.readlines()
    except FileNotFoundError:
        return 'not found'
    return {'filesource': filesource,
            'functionsource': functionsource}


_setuptext = """
You have succesfully logged in.
To avoid needing to type the server and username for each assignment you can
optionally create a configuration file to store this information. You can (1)
create the configuration file in the current directory, and keep/copy that
file into each directory from which you submit, or (2) create the configuration
file globally in:
    {homeconfig}

Please choose one of these options to create a configuration file:
    1. Create a local file "submissions.cfg".
    2. Create a universal file "{homeconfig}"
    3. Neither.
"""

_setuptext2 = """
Would you like to also store the password in this file?
WARNING: the password will be stored in plain text, so only do this on a
secure filesystem.

Please choose one option:
    1. Also save the password in the configuration file.
    2. Don't save the password (enter it manually each time).
"""
